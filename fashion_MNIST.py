import keras
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dense, Flatten, Dropout
from keras.optimizers import Adam
from keras.callbacks import TensorBoard


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

train_dataframe= pd.read_csv("fashion-mnist_train.csv")
test_dataframe= pd.read_csv("fashion-mnist_test.csv")

#print(train_dataframe.head())
#print(train_dataframe["pixel10"].max())
#egy kép= 28x28 pixel
# x=image/pixel data
# y=labels



#1) dataframe --> numpy array
train_data= np.array(train_dataframe, dtype="float32")
test_data= np.array(test_dataframe, dtype="float32")
#2) split arrays into x, y
x_train = train_data[:, 1:]/255
y_train = train_data[:, 0]

x_test = test_data[:, 1:]/255
y_test = test_data[:, 0]

#3) validation data
x_train, x_validate, y_train, y_validate = train_test_split(
	x_train ,y_train, test_size=0.2, random_state=12345
	)
image=x_train[50, :].reshape((28,28))

#plt.imshow(image)
#plt.show()

# 1.define model
# 2. compile
# 3. fit

im_rows = 28
im_cols = 28
batch_size = 512
im_shape = (im_rows, im_cols, 1)

x_train=x_train.reshape(x_train.shape[0], *im_shape)
x_test=x_test.reshape(x_test.shape[0], *im_shape)
x_validate=x_validate.reshape(x_validate.shape[0], *im_shape)

#define model
cnn_model = Sequential([
	Conv2D(filters=32, kernel_size=3, activation="relu", input_shape=im_shape),
	MaxPooling2D(pool_size=2),
	Dropout(0.2),

	Flatten(),
	Dense(32, activation="relu"),
	Dense(10, activation="softmax")#output layer
	])
#compile model
cnn_model.compile(
	loss="sparse_categorical_crossentropy",
	optimizer=Adam(lr=0.001),#learning rate
	metrics=["accuracy"]
	)
#fit model
cnn_model.fit(
	x_train,y_train,batch_size=batch_size,
	epochs=10,verbose=1,
	validation_data=(x_validate, y_validate),

	)

#evaluate model
score= cnn_model.evaluate(x_test,y_test, verbose=0)

print("test loss: {:.4f}".format(score[0]))
print("test accuracy: {:.4f}".format(score[1]))
#loss = 0.2888
#accuracy = 0.8969





















































